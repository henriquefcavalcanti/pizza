let btnIncrementas = document.querySelectorAll('.btn-incrementa')
let btnDecrementas = document.querySelectorAll('.btn-decrementa')

// INCREMENTANDO VALOR
for (let btn of btnIncrementas) {
    btn.addEventListener("click", incrementa)

    function incrementa() {
        let item = btn.closest('.item')
        let input = item.querySelector('.quantidade')
        input.value++

        let preco = pegaPrecoItem(item)
        addTotal(preco)
    }
}

// DECREMENTANDO VALOR
for (let btn of btnDecrementas) {
    btn.addEventListener('click', decrementa)

    function decrementa() {
        let item = btn.closest('.item')
        let input = item.querySelector('.quantidade')
        if (input.value > 0) {
            input.value--
            let preco = pegaPrecoItem(item)
            addTotal(-preco)
        }
    }
}

let formPedido = document.forms.pedido
formPedido.addEventListener('submit', (event) => {
    let contador = 0
    let inputs = formPedido.querySelectorAll('input.quantidade')
    for (let input of inputs) {
        if (input.value > 0) {
            contador++
        }
    }
    if (contador === 0) {
        alert('Deve ter pelo menos 1 pizza no pedido!')
        event.preventDefault()
    }
})




// FUNÇÕES AUXILIARES
function pegaPrecoItem(item) {
    let precoItem = item.querySelector('.preco-item')
    return (Number(precoItem.textContent))
}

function addTotal(preco) {
    let Total = document.querySelector('#total')
    Total.textContent = preco + Number(Total.textContent)
}
